---
title: raw data loops -- part 1
date: 2020 Feb 17
---

## Le numérique n’a rien de virtuel

[Raw Data Loops @ Cultures Numériques](http://culturesnumeriques.erg.be/spip.php?rubrique70)


## What is audio?

![](img/Tuning_fork_on_resonator.jpg)

Image by <a href="//commons.wikimedia.org/wiki/User:Brian0918" title="User:Brian0918">brian0918</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=1891967">Link</a>

## Tracing the waves...

![](img/Tuning_fork_on_carbon_black.jpg)

> A needle on a tuning fork carved these figures on a glass plate covered with carbon black. As the plate is moved from left to right, the sinewave-shaped swinging motion appears.

> Eine Nadel an der Spitze einer Stimmgabel ritze diese Figuren auf eine berußte Platte. Da die Stimmgabel dabei von links nach rechts bewegt wurde, ist eine sinusförmige Schwingung zu sehen.

Image by <a href="//commons.wikimedia.org/wiki/User:Kdkeller" title="User:Kdkeller">Klaus-Dieter Keller</a> - <span class="int-own-work" lang="en">Own work</span>, <a href="https://creativecommons.org/licenses/by-sa/3.0" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=15533712">Link</a>

## Waves (2)

![](img/james01.png)

![](img/james02.png)

![](img/james03.png)

![](img/james04.png)

![](img/james05.png)

Source: James Jeans, Science and Music (1937)



## Phonautograph


![](img/scott.jpg)

> Édouard-Léon Scott de Martinville invented sound recording when he conceived of a machine that would do for the ear what the camera did for the eye. His "phonautograph" inscribed airborne sounds onto paper, over time, to be studied visually. He called his recordings "phonautograms". Collections of his work lay silent and forgotten in venerable French institutions for 150 years—their provenance indisputable and their chain of custody uninterrupted. Following leads offered by First Sounds researcher Patrick Feaster and tips gleaned on the trail, David Giovannoni located six collections containing several dozen sound recordings made in Paris between c.1853 and 1860. Neither Scott de Martinville nor his contemporaries conceived of playing back his recordings; however, First Sounds—Patrick Feaster in particular—has coaxed nearly 20 to speak and sing to date.


* "le problème de la parole s'écrivant elle-même"
* "INSCRIPTION AUTOMATIQUE DES SONS DE L ’ AIR AU MOYEN D ’ UNE OREILLE ARTIFICIELLE (1861)"

![](img/pisko-p73-phonautograph.jpg)




Source: <http://www.firstsounds.org/research/scott.php>


![](img/scott/scott01.png)
![](img/scott/scott02.png)
![](img/scott/scott03.png)
![](img/scott/scott04.png)
![](img/scott/scott05.png)
![](img/scott/scott06.png)
![](img/scott/scott07.png)
![](img/scott/scott08.png)
![](img/scott/scott09.png)
![](img/scott/scott10.png)
![](img/scott/scott11.png)
![](img/scott/scott12.png)
![](img/scott/scott13.png)
![](img/scott/scott14.png)
![](img/scott/scott15.png)
![](img/scott/scott16.png)
![](img/scott/scott17.png)
![](img/scott/scott18.png)
![](img/scott/scott19.png)


## What is a digital audio file?

![](img/audacityfr01.png)

![](img/audacityfr02.png)

![](img/audacityfr03.png)

![](img/audacityfr04.png)



## "Wave" file

> La modulation par impulsion et codage ou MIC (en anglais Pulse Code Modulation), généralement abrégé en PCM est une représentation numérique d'un signal électrique résultant d'un processus de numérisation. Le signal est d'abord échantillonné, puis chaque échantillon est quantifié indépendamment des autres échantillons, et chacune des valeurs quantifiées est convertie en un code numérique1. Le traitement indépendant de chaque échantillon implique qu'il n'y a ni chiffrement, ni compression de données.

> On trouve des fichiers de données MIC (PCM) bruts notamment dans les applications audio. Dans les télécommunications (RTC ou VoIP), ce sont des flux : on transmet les blocs d'échantillons à la file, sans préciser le début ni la fin. Dans les systèmes stéréophoniques ou multicanaux, les blocs d'échantillons correspondant à chaque canal sont multiplexés. Les fichiers WAV, AIFF et BWF indiquent dans leur en-tête le type de codage des données. Le plus souvent, les données audio en modulation par impulsion et codage sont des fragments (chunks) multiplexés échantillon par échantillon. 

Source: <https://fr.wikipedia.org/wiki/Modulation_d%27impulsion_cod%C3%A9e>

Uncompressed digital audio is typically represented as a "waveform" ... a stream of samples (Échantillonnage) taken at a high frequency (44100 times a second) that measure in effect the pressure of the air for an imaginary ear or microphone. When the process is reversed (the samples are used to control the tension on a membrane in a speaker), an approximation of the original wave of sound is produced.

## Square wave

![](img/audacityfr05.png)

![](img/audacityfr06.png)

![](img/audacityfr07.png)




## Export: WAV

![](img/audacityfr08.png)

![](img/audacityfr09.png)

![](img/export_wav03.png)

See: <https://hexed.it/>


## Export: "Raw" (header-less) data

![](img/audacityfr10.png)

![](img/audacityfr11.png)

![](img/hexedit_square2.png)

See: <https://hexed.it/>


## arBITraire

![](img/hexedit_square2.png)

Formats and conventions allow these patterns of "ones and zeroes" (already an interpretation) to be framed and interpreted. Exporting / Importing files as "RAW" or "headerless" data is one way to strip away these frames and change the rules that interpret the bits.


## Optical Sound


Left: Movietone track with variable density. Right: Variable area track

<p><a href="https://commons.wikimedia.org/wiki/File:Optical-film-soundtrack.svg#/media/File:Optical-film-soundtrack.svg"><img src="img/1200px-Optical-film-soundtrack.svg.png" alt="Optical-film-soundtrack.svg"></a><br>By <a href="//commons.wikimedia.org/wiki/User:Iainf" title="User:Iainf">Iainf</a> 09:33, 12 July 2006 (UTC) - <span class="int-own-work" lang="en">Own work</span>, <a href="https://creativecommons.org/licenses/by/2.5" title="Creative Commons Attribution 2.5">CC BY 2.5</a>, <a href="https://commons.wikimedia.org/w/index.php?curid=941554">Link</a></p>


## Relating sample rate to image size

What is the relationship between the size of the image and the length of the audio?

Most computer sound cards run at 44100 hertz (so called "CD-quality" since it was used to encode music on compact discs)

 44100 = 2 * 2 * 3 * 3 * 5 * 5 * 7 * 7

So if every pixel is an audio sample, a 441 x 100 image would represent a second of audio.

 441 x 100 = 1 second

![](img/441x100.png)



## Example 1: Sound to Image


The size of the image doesn't really matter as the data gets read linearly from left to right, top to bottom.

![](img/gimp01.png)

![](img/gimp02.png)

![](img/gimp03.png)

![](img/gimp04.png)


## Exercise 1: Image to Sound

* Generate image in GIMP
* Export as... ".data"
* Import (as raw data) Audacity.

![](text.png){style="border: 1px solid gray"}

TIP: Try Changing the Image Mode (in GIMP) from RGB to MONO (and back again).

TIP: When importing in Audacity, try Unsigned 8 bit MONO (endian unimportant).

* Examples of filters that generate patterns...
* Use Inkscape to make more intricate patterns
* Command line examples (with sox)


## Exercise 2a: Sound to Image

Generate tone in Audacity, save as ".data" and open in GIMP.

TIP: When importing, be sure to adjust the width and (esp) height to import the entire duration of an audio file. You will see blank space when the heigth is longer than the data.
 
## Exercise 2b: Round trip

Sound to Image -- manipulate with GIMP/Image tools -- and return to sound

![](librivox-1min.png)

Example: Vertical flip audio



## Sources


* [LibriVox](https://librivox.org/)
* [Aporee](https://aporee.org/maps/) Field recordings from around the world
* [Freesound](https://freesound.org/browse/)
* [Wikicommons](https://commons.wikimedia.org/wiki/Main_Page)








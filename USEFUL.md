
## Understanding the WAV (RIFF) format

See [wav](wav.html)


## Header as frame

Digital files are like abstract pictures that have to be framed in order to know how to interpret them.

Try the following...

* Generate a square wave tone in Audacity.
* Export them as WAV of various formats (signed 16 bit, 32 bit float, and 8 bit unsigned)
* Open in the [hex editor](http://hexed.it).
* Follow the WAV header guide to decode the information in the file.
* Create audio files and export them as WAV using Audacity.
* Isolate the header (using the truncate command)
* Attach the header to other files, adjust necessary parts, and play them.
* Use sox to do the same...



Relating sample rate to image size
==========================================

What is the relationship between the size of the image and the length of the audio?

If each pixel is a single audio sample, then pixels per second equals the audio sampling rate.

So, for a 8kHz sample, 8000 pixels per second, a second could be represented by a square 89 pixels square. Or 1000 pixels wide and 8 pixels high.

<canvas width=1000 height=8 style="border: 1px solid black"></canvas>

For a sample at 44100 hertz fit to screen size of 1366x768.

44100 = 2 * 2 * 3 * 3 * 5 * 5 * 7 * 7

441 x 100 = 1 second


Play any file as it were audio
=====================

A sample audio file:

* <https://librivox.org/three-hundred-tang-poems-volume-1-by-various/>
* <https://ia800506.us.archive.org/2/items/300_tang_poems_vol_1_librivox/300tangpoems_vol_1_001_64kb.mp3>

Example first with an actual audio file....

    play --rate 22010 --bits 16 --channels 1 --encoding signed-integer -t raw 300tangpoems_vol_1_001_64kb.wav 

One small (but important) difference is that there's a little pop at the start sox plays the wav header (the original file played directly should not have this)

Try playing with different rate, bitrates, and channels. Each should just affect the speed.

Now try playing the original compressed data:

    play --rate 22010 --bits 16 --channels 1 --encoding signed-integer -t raw 300tangpoems_vol_1_001_64kb.mp3 


In general, this can be done with any file.

    play --rate 44100 --bits 16 --channels 2 --encoding signed-integer -t raw someimage.png

Alternatively, if you don't have the play command, you can use sox directly to create a wav format copy which should then be playable by your computer.

    sox --rate 44100 --bits 16 --channels 2 --encoding signed-integer someimage.png output.wav


TODO: play raw data with audacity .... (instead of reloadable ??)

## Sound to image and back again

Import in GIMP,
Import to export as raw data! (not png ?!)  

    play --rate 22010 --bits 16 --channels 1 --encoding signed-integer -t raw 300tangpoems_vol_1_001_64kb-imported-RGB-backagain.data 

NB ... it truncates the sound -- have sox commands cookbook for extracting pieces.

BIG differences between unsigned and signed interpretations.


Good results with import gray ... then export to data (RGB):

    play --rate 22010 --bits 8 --channels 1 --encoding unsigned-integer -t raw 300tangpoems-gray-export.data 


## Sound-> Image Manipulations

Imported to GIMP as data -- chose 16 bit little endian gray scale. Exported as PNG and played in web browser still!!!

Try different manipulations, starting with relatively simple and singular ones to get their effects in isolation.

* movement, scaling
* flipping
* overlaying
* rotation (!)


## Resizing pbm --- fat pixels
Ludi's example played in browser -- stretched and tonal


## Optical sound

[Optical Sound Track Method](http://www.phonozoic.net/ostm1.html) (phonozoic)

Try edge detection on (complex) images ?!

## Canvas code

http://beej.us/blog/data/html5s-canvas-2-pixel/



## MIDI

https://medium.com/swinginc/playing-with-midi-in-javascript-b6999f2913c3

"Vinny" https://www.youtube.com/watch?v=p2oDIqajdFQ

https://gumroad.com/l/ZiGjZ

https://galactic.ink/midi-js/

* crazy https://galactic.ink/midi-js/WhitneyMusicBox.html

What is the relationship between the size of the image and the length of the audio?

If each pixel is a single audio sample, then pixels per second equals the audio sampling rate.

So, for a 8kHz sample, 8000 pixels per second, a second could be represented by a square 89 pixels square. Or 1000 pixels wide and 8 pixels high.

<canvas width=1000 height=8 style="border: 1px solid black"></canvas>

For a sample at 44100 hertz fit to screen size of 1366x768.

44100 = 2 * 2 * 3 * 3 * 5 * 5 * 7 * 7

441 x 100 = 1 second


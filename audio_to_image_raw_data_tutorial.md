In audacity, generate a sine wave at 440 Hz for 2 seconds (the default).

![](img/Audacity_8khz_sine.png)

Save as 8-bit unsigned data (ending in .data).

Open in GIMP as 8-bit gray. Play with the image width until you get near a vertical stripe. The width is now close to a multiple of the audio rate.

![](data/8khz-unsigned-8-bit-pcm.stripes.png)
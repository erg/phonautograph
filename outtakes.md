Create a reusable wav header
=================================
```
cp square-unsigned-8bit.wav unsigned-8bit.wav
truncate -s 44 unsigned-8bit.wav
```
To use this, though, you would need to update the file size, and data size bytes of the final file.
